<?php

declare(strict_types=1);

namespace App\Entity;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class Cell
{
    /**
     * @var UuidInterface unique ID of the cell
     */
    private UuidInterface $id;

    /**
     * @var Board Reference to the Board
     */
    private Board $board;

    /**
     * @var int X coordinate within the Board
     */
    private int $xCoordinate;

    /**
     * @var int Y coordinate within the Board
     */
    private int $yCoordinate;

    /**
     * @var bool if cell is not yet clicked
     */
    private bool $isHidden = true;

    /**
     * @var bool if cell has Black Hole
     */
    private bool $hasBlackHole;

    /**
     * @var bool if at least one adjacent cell is not yet clicked
     */
    private bool $hasHiddenNeighbour = true;

    /**
     * @var int the number of adjacent Black Holes for clicked cell
     */
    private int $adjacentBlackHoles;

    /**
     * @param Board $board
     * @param int $xCoordinate
     * @param int $yCoordinate
     * @param bool $hasBlackHole
     */
    public function __construct(Board $board, int $xCoordinate, int $yCoordinate, bool $hasBlackHole)
    {
        $this->id = Uuid::uuid4();
        $this->board = $board;
        $this->xCoordinate = $xCoordinate;
        $this->yCoordinate = $yCoordinate;
        $this->hasBlackHole = $hasBlackHole;
    }


}