<?php

declare(strict_types=1);

namespace App\Entity;

use App\Enum\Board\BlackHolesDistributionType;
use App\Exception\BlackHolesExceedingException;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class Board
{
    /**
     * @var UuidInterface unique ID of the board
     */
    private UuidInterface $id;

    /**
     * @var Game Reference to the Game
     */
    private Game $game;

    /**
     * @var int how many columns
     */
    private int $columns;

    /**
     * @var int how many rows
     */
    private int $rows;

    /**
     * @var int how many Black Holes
     */
    private int $blackHoles;

    /**
     * @var BlackHolesDistributionType how Black Holes are distributed within the board
     */
    private BlackHolesDistributionType $blackHolesDistributionType;

    /**
     * Since the Black Holes number is an input parameter, we are using probability function to decide
     * whether to put Black Hole in every single cell or not. Therefore, the might be situation when real Black Holes
     * number is not equal to initial. This case considered as unsuccessful generation. If this happened, the generation
     * starts again and again.
     *
     * @var int how many cells generations happened until initial $blackHoles is equal to what we got
     *
     */
    private int $successfulGenCounter = 0;

    /**
     * @var float Total time till successful generation millisecond
     */
    private float $successfulGenTime = 0;

    /**
     * @param Game $game
     * @param int $columns
     * @param int $rows
     * @param int $blackHoles
     * @param BlackHolesDistributionType $blackHolesDistributionType
     */
    public function __construct(
        Game $game,
        int $columns,
        int $rows,
        int $blackHoles,
        BlackHolesDistributionType $blackHolesDistributionType
    )
    {
        $this->id = Uuid::uuid4();
        $this->game = $game;

        if (!$this->blackHolesNumberIsValid($columns, $rows, $blackHoles)) {
            throw new BlackHolesExceedingException();
        }

        $this->columns = $columns;
        $this->rows = $rows;
        $this->blackHoles = $blackHoles;
        $this->blackHolesDistributionType = $blackHolesDistributionType;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id->toString();
    }

    /**
     * @return Game
     */
    public function getGame(): Game
    {
        return $this->game;
    }

    /**
     * @return int
     */
    public function getColumns(): int
    {
        return $this->columns;
    }

    /**
     * @return int
     */
    public function getRows(): int
    {
        return $this->rows;
    }

    /**
     * @return int
     */
    public function getBlackHoles(): int
    {
        return $this->blackHoles;
    }

    /**
     * @return string
     */
    public function getBlackHolesDistributionType(): string
    {
        return $this->blackHolesDistributionType->value;
    }

    /**
     * @return int
     */
    public function getSuccessfulGenCounter(): int
    {
        return $this->successfulGenCounter;
    }

    public function incrementSuccessfulGenCounter(): void
    {
        $this->successfulGenCounter++;
    }

    /**
     * @return float
     */
    public function getSuccessfulGenTime(): float
    {
        return $this->successfulGenTime;
    }

    /**
     * @param float $successfulGenTime
     */
    public function setSuccessfulGenTime(float $successfulGenTime): void
    {
        $this->successfulGenTime = $successfulGenTime;
    }

    /**
     * @param int $columns
     * @param int $rows
     * @param int $blackHoles
     * @return bool
     */
    private function blackHolesNumberIsValid(int $columns, int $rows, int $blackHoles): bool
    {
        return $blackHoles <= $columns * $rows;
    }
}