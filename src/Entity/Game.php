<?php

declare(strict_types=1);

namespace App\Entity;

use App\Enum\Game\Status;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class Game
{
    /**
     * @var UuidInterface unique ID of the game
     */
    private UuidInterface $id;

    /**
     * @var Status status to describe most important stages of the game
     */
    private Status $status;

    public function __construct()
    {
        $this->id = Uuid::uuid4();
        $this->status = Status::InProgress;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id->toString();
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status->value;
    }

    /**
     * @param Status $status
     */
    public function setStatus(Status $status): void
    {
        $this->status = $status;
    }
}