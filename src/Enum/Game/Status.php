<?php

declare(strict_types=1);

namespace App\Enum\Game;

enum Status: string
{
    case InProgress = 'In progress';
    case Failed = 'Failed';
    case Won = 'Won';
}
