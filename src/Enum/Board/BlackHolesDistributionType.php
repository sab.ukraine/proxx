<?php

declare(strict_types=1);

namespace App\Enum\Board;

enum BlackHolesDistributionType: string
{
    case Uniform = 'Uniform';
}