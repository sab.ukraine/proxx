<?php

declare(strict_types=1);

namespace App\Exception;

class BlackHolesExceedingException extends \RuntimeException
{
    public function __construct()
    {
        parent::__construct('The number of Black Holes can not be more than total amount of cells of the board.');
    }
}