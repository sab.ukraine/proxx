<?php

declare(strict_types=1);

namespace App\Command;

use App\Entity\Board;
use App\Entity\Cell;
use App\Entity\Game;
use App\Enum\Board\BlackHolesDistributionType;
use App\Exception\BlackHolesExceedingException;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:start-game',
    description: 'Starts a new game.',
    aliases: ['app:new-game'],
    hidden: false
)]
class StartGameCommand extends Command
{
    /**
     * Configures the current command.
     *
     * @return void
     */
    protected function configure(): void
    {
        $this
            ->setHelp('This command allows you to start a new game.')
        ;
    }

    /**
     * Executes the current command.
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $helper = $this->getHelper('question');
        $questionColumns = new Question('Provide a board columns number: ', '10');
        $columns = (int) $helper->ask($input, $output, $questionColumns);
        $questionRows = new Question('Provide a board rows number: ', '10');
        $rows = (int) $helper->ask($input, $output, $questionRows);
        $questionBlackHoles = new Question('Provide a board Black Holes number: ', '10');
        $blackHoles = (int) $helper->ask($input, $output, $questionBlackHoles);

        $game = new Game();
        try {
            $board = new Board(
                $game,
                $columns,
                $rows,
                $blackHoles,
                BlackHolesDistributionType::Uniform
            );
        } catch (BlackHolesExceedingException $exception) {
            $output->writeln('<error>' . $exception->getMessage() . '</error>');

            return self::FAILURE;
        }


        $output->writeln('<info>Game ID: </info>' . $game->getId());
        $output->writeln('<info>Game status: </info>' . $game->getStatus());
        $output->writeln('<info>Board ID: </info>' . $board->getId());
        $output->writeln('<info>Board columns: </info>' . $board->getColumns());
        $output->writeln('<info>Board rows: </info>' . $board->getRows());
        $output->writeln('<info>Board Black Holes: </info>' . $board->getBlackHoles());

        $blackHoleProbability = $board->getBlackHoles() / $board->getColumns() / $board->getRows() * 100;

        $generate = true;
        $startTime = microtime(true);
        while($generate){
            $blackHolesCounter = 0;
            for ($x = 1; $x <= $columns; $x++) {
                for ($y = 1; $y <= $rows; $y++) {
                    $hasBlackHole = false;
                    if (mt_rand( 0, 100 ) < $blackHoleProbability) {
                        $hasBlackHole = true;
                        $blackHolesCounter++;
                    }

                    $cellObject = new Cell($board, $y, $x, $hasBlackHole);
                    $cellsArray[$y][$x] = [
                        'x' => $x,
                        'y' => $y,
                        'hasBlackHole' => $hasBlackHole
                    ];

                    if ($hasBlackHole) {
                        $cellsArray[$y][$x]['adjacentBlackHoles'] = null;
                        $cellsArrayToRender[$y][$x] = "($x, $y) H";

                        //bottom-right
                        $neighbourX = $x+1;
                        $neighbourY = $y+1;
                        if (
                            $neighbourX >= 1
                            &&
                            $neighbourY >= 1
                            &&
                            $neighbourX <= $columns
                            &&
                            $neighbourY <= $rows
                        ) {
                            if (array_key_exists($neighbourY, $cellsArray) && array_key_exists($neighbourX, $cellsArray[$neighbourY])) {
                                if (array_key_exists('hasBlackHole', $cellsArray[$neighbourY][$neighbourX])) {
                                    if (!$cellsArray[$neighbourY][$neighbourX]['hasBlackHole']) {
                                        if (array_key_exists('adjacentBlackHoles', $cellsArray[$neighbourY][$neighbourX])) {
                                            if (null === $cellsArray[$neighbourY][$neighbourX]['adjacentBlackHoles']) {
                                                $cellsArrayToRender[$neighbourY][$neighbourX] = "($neighbourX, $neighbourY) H";
                                            } else {
                                                $updated = $cellsArray[$neighbourY][$neighbourX]['adjacentBlackHoles']++;
                                                $cellsArrayToRender[$neighbourY][$neighbourX] = "($neighbourX, $neighbourY) $updated";
                                            }
                                        } else {
                                            $cellsArray[$neighbourY][$neighbourX]['adjacentBlackHoles'] = 1;
                                            $cellsArrayToRender[$neighbourY][$neighbourX] = "($neighbourX, $neighbourY) 1";
                                        }
                                    }
                                } else {
                                    $updated = $cellsArray[$neighbourY][$neighbourX]['adjacentBlackHoles']++;
                                    $cellsArrayToRender[$neighbourY][$neighbourX] = "($neighbourX, $neighbourY) $updated";
                                }
                            } else {
                                $cellsArray[$neighbourY][$neighbourX]['adjacentBlackHoles'] = 1;
                                $cellsArrayToRender[$neighbourY][$neighbourX] = "($neighbourX, $neighbourY) 1";
                            }
                        }
                        //top-left
                        $neighbourX = $x-1;
                        $neighbourY = $y-1;
                        if (
                            $neighbourX >= 1
                            &&
                            $neighbourY >= 1
                            &&
                            $neighbourX <= $columns
                            &&
                            $neighbourY <= $rows
                        ) {
                            if (array_key_exists($neighbourY, $cellsArray) && array_key_exists($neighbourX, $cellsArray[$neighbourY])) {
                                if (array_key_exists('hasBlackHole', $cellsArray[$neighbourY][$neighbourX])) {
                                    if (!$cellsArray[$neighbourY][$neighbourX]['hasBlackHole']) {
                                        if (array_key_exists('adjacentBlackHoles', $cellsArray[$neighbourY][$neighbourX])) {
                                            if (null === $cellsArray[$neighbourY][$neighbourX]['adjacentBlackHoles']) {
                                                $cellsArrayToRender[$neighbourY][$neighbourX] = "($neighbourX, $neighbourY) H";
                                            } else {
                                                $updated = $cellsArray[$neighbourY][$neighbourX]['adjacentBlackHoles']++;
                                                $cellsArrayToRender[$neighbourY][$neighbourX] = "($neighbourX, $neighbourY) $updated";
                                            }
                                        } else {
                                            $cellsArray[$neighbourY][$neighbourX]['adjacentBlackHoles'] = 1;
                                            $cellsArrayToRender[$neighbourY][$neighbourX] = "($neighbourX, $neighbourY) 1";
                                        }
                                    }
                                } else {
                                    $updated = $cellsArray[$neighbourY][$neighbourX]['adjacentBlackHoles']++;
                                    $cellsArrayToRender[$neighbourY][$neighbourX] = "($neighbourX, $neighbourY) $updated";
                                }
                            } else {
                                $cellsArray[$neighbourY][$neighbourX]['adjacentBlackHoles'] = 1;
                                $cellsArrayToRender[$neighbourY][$neighbourX] = "($neighbourX, $neighbourY) 1";
                            }
                        }
                        //bottom-left
                        $neighbourX = $x-1;
                        $neighbourY = $y+1;
                        if (
                            $neighbourX >= 1
                            &&
                            $neighbourY >= 1
                            &&
                            $neighbourX <= $columns
                            &&
                            $neighbourY <= $rows
                        ) {
                            if (array_key_exists($neighbourY, $cellsArray) && array_key_exists($neighbourX, $cellsArray[$neighbourY])) {
                                if (array_key_exists('hasBlackHole', $cellsArray[$neighbourY][$neighbourX])) {
                                    if (!$cellsArray[$neighbourY][$neighbourX]['hasBlackHole']) {
                                        if (array_key_exists('adjacentBlackHoles', $cellsArray[$neighbourY][$neighbourX])) {
                                            if (null === $cellsArray[$neighbourY][$neighbourX]['adjacentBlackHoles']) {
                                                $cellsArrayToRender[$neighbourY][$neighbourX] = "($neighbourX, $neighbourY) H";
                                            } else {
                                                $updated = $cellsArray[$neighbourY][$neighbourX]['adjacentBlackHoles']++;
                                                $cellsArrayToRender[$neighbourY][$neighbourX] = "($neighbourX, $neighbourY) $updated";
                                            }
                                        } else {
                                            $cellsArray[$neighbourY][$neighbourX]['adjacentBlackHoles'] = 1;
                                            $cellsArrayToRender[$neighbourY][$neighbourX] = "($neighbourX, $neighbourY) 1";
                                        }
                                    }
                                } else {
                                    $updated = $cellsArray[$neighbourY][$neighbourX]['adjacentBlackHoles']++;
                                    $cellsArrayToRender[$neighbourY][$neighbourX] = "($neighbourX, $neighbourY) $updated";
                                }
                            } else {
                                $cellsArray[$neighbourY][$neighbourX]['adjacentBlackHoles'] = 1;
                                $cellsArrayToRender[$neighbourY][$neighbourX] = "($neighbourX, $neighbourY) 1";
                            }
                        }
                        //top-right
                        $neighbourX = $x+1;
                        $neighbourY = $y-1;
                        if (
                            $neighbourX >= 1
                            &&
                            $neighbourY >= 1
                            &&
                            $neighbourX <= $columns
                            &&
                            $neighbourY <= $rows
                        ) {
                            if (array_key_exists($neighbourY, $cellsArray) && array_key_exists($neighbourX, $cellsArray[$neighbourY])) {
                                if (array_key_exists('hasBlackHole', $cellsArray[$neighbourY][$neighbourX])) {
                                    if (!$cellsArray[$neighbourY][$neighbourX]['hasBlackHole']) {
                                        if (array_key_exists('adjacentBlackHoles', $cellsArray[$neighbourY][$neighbourX])) {
                                            if (null === $cellsArray[$neighbourY][$neighbourX]['adjacentBlackHoles']) {
                                                $cellsArrayToRender[$neighbourY][$neighbourX] = "($neighbourX, $neighbourY) H";
                                            } else {
                                                $updated = $cellsArray[$neighbourY][$neighbourX]['adjacentBlackHoles']++;
                                                $cellsArrayToRender[$neighbourY][$neighbourX] = "($neighbourX, $neighbourY) $updated";
                                            }
                                        } else {
                                            $cellsArray[$neighbourY][$neighbourX]['adjacentBlackHoles'] = 1;
                                            $cellsArrayToRender[$neighbourY][$neighbourX] = "($neighbourX, $neighbourY) 1";
                                        }
                                    }
                                } else {
                                    $updated = $cellsArray[$neighbourY][$neighbourX]['adjacentBlackHoles']++;
                                    $cellsArrayToRender[$neighbourY][$neighbourX] = "($neighbourX, $neighbourY) $updated";
                                }
                            } else {
                                $cellsArray[$neighbourY][$neighbourX]['adjacentBlackHoles'] = 1;
                                $cellsArrayToRender[$neighbourY][$neighbourX] = "($neighbourX, $neighbourY) 1";
                            }
                        }
                        //right
                        $neighbourX = $x+1;
                        $neighbourY = $y;
                        if (
                            $neighbourX >= 1
                            &&
                            $neighbourY >= 1
                            &&
                            $neighbourX <= $columns
                            &&
                            $neighbourY <= $rows
                        ) {
                            if (array_key_exists($neighbourY, $cellsArray) && array_key_exists($neighbourX, $cellsArray[$neighbourY])) {
                                if (array_key_exists('hasBlackHole', $cellsArray[$neighbourY][$neighbourX])) {
                                    if (!$cellsArray[$neighbourY][$neighbourX]['hasBlackHole']) {
                                        if (array_key_exists('adjacentBlackHoles', $cellsArray[$neighbourY][$neighbourX])) {
                                            if (null === $cellsArray[$neighbourY][$neighbourX]['adjacentBlackHoles']) {
                                                $cellsArrayToRender[$neighbourY][$neighbourX] = "($neighbourX, $neighbourY) H";
                                            } else {
                                                $updated = $cellsArray[$neighbourY][$neighbourX]['adjacentBlackHoles']++;
                                                $cellsArrayToRender[$neighbourY][$neighbourX] = "($neighbourX, $neighbourY) $updated";
                                            }
                                        } else {
                                            $cellsArray[$neighbourY][$neighbourX]['adjacentBlackHoles'] = 1;
                                            $cellsArrayToRender[$neighbourY][$neighbourX] = "($neighbourX, $neighbourY) 1";
                                        }
                                    }
                                } else {
                                    $updated = $cellsArray[$neighbourY][$neighbourX]['adjacentBlackHoles']++;
                                    $cellsArrayToRender[$neighbourY][$neighbourX] = "($neighbourX, $neighbourY) $updated";
                                }
                            } else {
                                $cellsArray[$neighbourY][$neighbourX]['adjacentBlackHoles'] = 1;
                                $cellsArrayToRender[$neighbourY][$neighbourX] = "($neighbourX, $neighbourY) 1";
                            }
                        }
                        //left
                        $neighbourX = $x-1;
                        $neighbourY = $y;
                        if (
                            $neighbourX >= 1
                            &&
                            $neighbourY >= 1
                            &&
                            $neighbourX <= $columns
                            &&
                            $neighbourY <= $rows
                        ) {
                            if (array_key_exists($neighbourY, $cellsArray) && array_key_exists($neighbourX, $cellsArray[$neighbourY])) {
                                if (array_key_exists('hasBlackHole', $cellsArray[$neighbourY][$neighbourX])) {
                                    if (!$cellsArray[$neighbourY][$neighbourX]['hasBlackHole']) {
                                        if (array_key_exists('adjacentBlackHoles', $cellsArray[$neighbourY][$neighbourX])) {
                                            if (null === $cellsArray[$neighbourY][$neighbourX]['adjacentBlackHoles']) {
                                                $cellsArrayToRender[$neighbourY][$neighbourX] = "($neighbourX, $neighbourY) H";
                                            } else {
                                                $updated = $cellsArray[$neighbourY][$neighbourX]['adjacentBlackHoles']++;
                                                $cellsArrayToRender[$neighbourY][$neighbourX] = "($neighbourX, $neighbourY) $updated";
                                            }
                                        } else {
                                            $cellsArray[$neighbourY][$neighbourX]['adjacentBlackHoles'] = 1;
                                            $cellsArrayToRender[$neighbourY][$neighbourX] = "($neighbourX, $neighbourY) 1";
                                        }
                                    }
                                } else {
                                    $updated = $cellsArray[$neighbourY][$neighbourX]['adjacentBlackHoles']++;
                                    $cellsArrayToRender[$neighbourY][$neighbourX] = "($neighbourX, $neighbourY) $updated";
                                }
                            } else {
                                $cellsArray[$neighbourY][$neighbourX]['adjacentBlackHoles'] = 1;
                                $cellsArrayToRender[$neighbourY][$neighbourX] = "($neighbourX, $neighbourY) 1";
                            }
                        }
                        //bottom
                        $neighbourX = $x;
                        $neighbourY = $y+1;
                        if (
                            $neighbourX >= 1
                            &&
                            $neighbourY >= 1
                            &&
                            $neighbourX <= $columns
                            &&
                            $neighbourY <= $rows
                        ) {
                            if (array_key_exists($neighbourY, $cellsArray) && array_key_exists($neighbourX, $cellsArray[$neighbourY])) {
                                if (array_key_exists('hasBlackHole', $cellsArray[$neighbourY][$neighbourX])) {
                                    if (!$cellsArray[$neighbourY][$neighbourX]['hasBlackHole']) {
                                        if (array_key_exists('adjacentBlackHoles', $cellsArray[$neighbourY][$neighbourX])) {
                                            if (null === $cellsArray[$neighbourY][$neighbourX]['adjacentBlackHoles']) {
                                                $cellsArrayToRender[$neighbourY][$neighbourX] = "($neighbourX, $neighbourY) H";
                                            } else {
                                                $updated = $cellsArray[$neighbourY][$neighbourX]['adjacentBlackHoles']++;
                                                $cellsArrayToRender[$neighbourY][$neighbourX] = "($neighbourX, $neighbourY) $updated";
                                            }
                                        } else {
                                            $cellsArray[$neighbourY][$neighbourX]['adjacentBlackHoles'] = 1;
                                            $cellsArrayToRender[$neighbourY][$neighbourX] = "($neighbourX, $neighbourY) 1";
                                        }
                                    }
                                } else {
                                    $updated = $cellsArray[$neighbourY][$neighbourX]['adjacentBlackHoles']++;
                                    $cellsArrayToRender[$neighbourY][$neighbourX] = "($neighbourX, $neighbourY) $updated";
                                }
                            } else {
                                $cellsArray[$neighbourY][$neighbourX]['adjacentBlackHoles'] = 1;
                                $cellsArrayToRender[$neighbourY][$neighbourX] = "($neighbourX, $neighbourY) 1";
                            }
                        }
                        //top
                        $neighbourX = $x;
                        $neighbourY = $y-1;
                        if (
                            $neighbourX >= 1
                            &&
                            $neighbourY >= 1
                            &&
                            $neighbourX <= $columns
                            &&
                            $neighbourY <= $rows
                        ) {
                            if (array_key_exists($neighbourY, $cellsArray) && array_key_exists($neighbourX, $cellsArray[$neighbourY])) {
                                if (array_key_exists('hasBlackHole', $cellsArray[$neighbourY][$neighbourX])) {
                                    if (!$cellsArray[$neighbourY][$neighbourX]['hasBlackHole']) {
                                        if (array_key_exists('adjacentBlackHoles', $cellsArray[$neighbourY][$neighbourX])) {
                                            if (null === $cellsArray[$neighbourY][$neighbourX]['adjacentBlackHoles']) {
                                                $cellsArrayToRender[$neighbourY][$neighbourX] = "($neighbourX, $neighbourY) H";
                                            } else {
                                                $updated = $cellsArray[$neighbourY][$neighbourX]['adjacentBlackHoles']++;
                                                $cellsArrayToRender[$neighbourY][$neighbourX] = "($neighbourX, $neighbourY) $updated";
                                            }
                                        } else {
                                            $cellsArray[$neighbourY][$neighbourX]['adjacentBlackHoles'] = 1;
                                            $cellsArrayToRender[$neighbourY][$neighbourX] = "($neighbourX, $neighbourY) 1";
                                        }
                                    }
                                } else {
                                    $updated = $cellsArray[$neighbourY][$neighbourX]['adjacentBlackHoles']++;
                                    $cellsArrayToRender[$neighbourY][$neighbourX] = "($neighbourX, $neighbourY) $updated";
                                }
                            } else {
                                $cellsArray[$neighbourY][$neighbourX]['adjacentBlackHoles'] = 1;
                                $cellsArrayToRender[$neighbourY][$neighbourX] = "($neighbourX, $neighbourY) 1";
                            }
                        }
                    } else {
                        if (!array_key_exists('adjacentBlackHoles', $cellsArray[$y][$x])) {
                            $cellsArray[$y][$x]['adjacentBlackHoles'] = 0;
                            $cellsArrayToRender[$y][$x] = "($x, $y) 0";
                        }
                    }
                }
            }
            $board->incrementSuccessfulGenCounter();
            if ($blackHolesCounter === $board->getBlackHoles()) {
                $generate = false;
            }
        }
        $endTime = microtime(true);
        $executionTimeMs = ($endTime - $startTime) * 1000;
        $board->setSuccessfulGenTime($executionTimeMs);



        $io = new SymfonyStyle($input, $output);
        $io->createTable()->addRows(
            $cellsArrayToRender
        )->render();

        $output->writeln('<info>Board generation attempts: </info>' . $board->getSuccessfulGenCounter());
        $output->writeln('<info>Board generation time: </info>' . $board->getSuccessfulGenTime());

        return self::SUCCESS;
    }
}